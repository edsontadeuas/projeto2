variable "ec2_region" {
    default = "sa-east-1"
}

variable "ec2_keypair_name" {
    default = "chave-gitlab"
}

variable "ec2_instance_type" {
    default = "t2.micro"
}

variable "ec2_image_id" {
    default = "ami-090006f29ecb2d79a"
}

variable "ec2_tags" {
    default = "Descomplicando o Gitlab"
}

variable "ec2_instance_count" {
    default = "1"
}